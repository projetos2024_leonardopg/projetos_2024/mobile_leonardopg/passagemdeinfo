import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import TaskList from './src/TaskList';
import TaskDetails from './src/TaskDetails';

const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="">
        <Stack.Screen name="ListaDeTarefas" component={TaskList}/>
        <Stack.Screen name="DetalhesDasTarefas" component={TaskDetails} options={{title: "Informação das Tarefas"}}/>
      </Stack.Navigator>
    </NavigationContainer>
  );
}