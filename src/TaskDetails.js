import react from "react";
import { Text, View, TouchableOpacity, FlatList } from "react-native";

const TaskDetails = ({ route }) => {
  const { task } = route.params;

  return (
    <View>
      <Text>Detalhes de Tarefas</Text>
      <Text>Data: {task.date}</Text>
      <Text>Hora: {task.time}</Text>
      <Text>Local: {task.address}</Text>
    </View>
  );
};
export default TaskDetails;
