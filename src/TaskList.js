import react from "react";
import { Text, View, TouchableOpacity, FlatList } from "react-native";

const TaskList = ({ navigation }) => {
  const tasks = [
    {
      id: 1,
      title: "ir ao mercado",
      date: "2024-02-27",
      time: "10:00",
      address: "supermercado SS",
    },
    {
      id: 2,
      title: "fazer exercícios",
      date: "2024-02-28",
      time: "08:00",
      address: "academia local",
    },
    {
      id: 3,
      title: "ligar para o médico",
      date: "2024-02-29",
      time: "15:30",
      address: "hospital municipal",
    },
  ];

  const taskPress = (task) => {
    navigation.navigate("DetalhesDasTarefas",{task})
  };

  return (
    <View>
      <FlatList
        data={tasks}
        keyExtractor={(item) => item.id.toString}
        renderItem={({ item }) => (
          <TouchableOpacity onPress={() => taskPress(item)}>
            <Text>{item.title}</Text>
          </TouchableOpacity>
        )}
      />
    </View>
  );
};
export default TaskList;
